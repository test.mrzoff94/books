<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php 
        echo Yii::$app->User->can('admin') ? Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) : ''; 
        ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            [
                'label' => 'Автор',
                'content' => function($model){
                    return $model->getAuthor()->one()->authorname;
                }
            ],
            [
                'label' => 'Дата создания',
                'content' => function($model){
                    return Yii::$app->formatter->asDate($model->created_at, 'yyyy-MM-dd');
                }
            ],
            [
                'class' => ActionColumn::className(),
                'visible' =>  Yii::$app->User->can('admin'),
            ],
        ],
    ]); ?>


</div>

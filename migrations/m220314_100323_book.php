<?php

use yii\db\Migration;

/**
 * Class m220314_100323_book
 */
class m220314_100323_book extends Migration
{
    public function safeUp()
    {
        $this->createTable('book', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
            'author' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            '{{%fk-book-author}}',
            '{{%book}}',
            'author',
            '{{%author}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('book');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220314_100603_book cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m220314_100224_author
 */
class m220314_100224_author extends Migration
{
    public function safeUp()
    {
        $this->createTable('author', [
            'id' => $this->primaryKey(),
            'authorname' => $this->string()->notNull()->unique(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('author');
    }
}
